$(function() {

	$("[id=modifyAddress]").click(function() {
		window.location = "../ShoppingCart/modifyCheckOutAddress.php?id=checkOut";
	});

	$("[id=modifyPayment]").click(function() {
		window.location = "../ShoppingCart/checkOutPayment.php";
	});
	
	$("[class=undo]").click(function() {
		$.post("../../ApiAJAX/ShoppingCart/cancelOrder.php");
		window.location = "../ShoppingCart/userShoppingCart.php";
	});

	$("[class=conclude]").click(function() {
		$.post("../../ApiAJAX/ShoppingCart/setSessionVar.php");
		window.location = "../ShoppingCart/orderConcluded.php";
	});

});