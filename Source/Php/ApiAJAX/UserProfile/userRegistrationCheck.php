<?php

require_once("../../Site/baseConfiguration.php");

if(isUserLoggedIn() || isSellerLoggedIn()) {
	header("Location: ../../Site/Errors/errors.php?errorType=401&errorMessage=È necessario essere loggato come utente per poter compiere questa operazione");
}

if(isset($_POST["name"]) && isset($_POST["email"]) && isset($_POST["pw"]) && isset($_POST["confirmpw"])) {
	$errors = null;

	if(!checkEmailValidity($_POST["email"])) {
		$errors[0] = "FORMATO E-MAIL SBAGLIATO O E-MAIL GIÀ UTILIZZATA";
	}

	if(!checkPasswordValidity($_POST["pw"])) {
		$errors[1] = "LA PASSWORD DEVE ESSERE COMPRESA FRA 8 E 32 CARATTERI";
	}

	if(strcmp($_POST["pw"], $_POST["confirmpw"]) !== 0) {
		$errors[2] = "LA PASSWORD NON È STATA CONFERMATA CORRETTAMENTE";
	}

	if(!isset($_POST["confirmage"]) || $_POST["confirmage"] != "confirm-adult-age") {
		$errors[3] = "DEVI CONFERMARE DI ESSERE MAGGIORENNE PER POTERTI REGISTRARE";
	}

	if(!checkNameValidity($_POST["name"])) {
		$errors[4] = "IL CAMPO 'NOME' DEVE ESSERE LUNGO MASSIMO 30 CARATTERI";
	}

	if(!isset($errors)) {
		$dbh->registerUser($_POST["name"], $_POST["email"], $_POST["pw"]);
		$user = $dbh->checkUserLogin($_POST["email"], $_POST["pw"]);
		registerLoggedUser($user[0]["IdUtente"]);
	}

	echo json_encode($errors);
}