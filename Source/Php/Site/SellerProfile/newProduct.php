<?php

require_once("../baseConfiguration.php");

if(!isSellerLoggedIn()){
	header("Location: ../Login/login.php");
} else {
	$templateParams["Titolo"] = "ProfilePage - AddNewProduct";
	$templateParams["Javascript"] = array("SellerProfile/sellerProfilenav.js", "SellerProfile/newProduct.js");
	$templateParams["Template"] = "SellerProfile/newProductTemplate.php";
	$templateParams["HeaderType"] = "Profile"; //Profile, Login, Global

	require("../../Template/baseTemplate.php");
}

?>