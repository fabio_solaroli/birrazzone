<?php 

if(isset($_GET["id"]) && !empty($dbh->getBeer($_GET["id"]))){
	$idbirra = $_GET["id"];
} else {
	header("Location: ../../Site/Errors/errors.php?errorType=404&errorMessage=La birra cercata è inesistente");
}

if(isset($_GET["quantity"])) {
	$defaultQuantity = $_GET["quantity"];
} else {
	$defaultQuantity = 1;
}

$birra = $dbh->getBeer($idbirra)[0];

$birrificio = $dbh->getSeller($birra["IdBirrificio"])[0];
$quantitaBirra = $dbh->getBeerQuantity($birra["IdBirra"]);
?>
<h1><?php echo $birra["NomeBirra"];?></h1>
<section>
	<ul>
		<li id="productId" hidden><?php echo $birra["IdBirra"]; ?></li>
		<li class="center"><?php echo $birra["Tipologia"];?></li>
		<li class="photo"><img class="beerPhoto" src="<?php echo UPLOAD_DIR."Birre/".$birra["FotoBirra"] ?>" alt="FotoBirra"/></li>
	</ul>
</section>
<section>
	<section class="overflow">
		<ul>
			<li class="center"><a href="../SellerPage/sellerPage.php?sellerId=<?php echo $birrificio["IdBirrificio"]?>"><?php echo $birrificio["NomeBirrificio"] ?></a></li>
			<li><?php echo $birra["Prezzo"];?>€ cad.</li>
			<li><span>Formato: </span><span><?php echo $birra["Formato"];?></span></li>
			<li><span>Grado Alcolico: </span><span><?php echo $birra["GradoAlcolico"];?></span></li>
			<li><span>Colore: </span><span><?php echo $birra["Colore"];?></span></li>
			<li><span>Descrizione: </span><span><?php echo $birra["DescrizioneBirra"];?></span></li>
		</ul>
	</section>
	<section class="addToCart">
		<?php if(!isSellerLoggedIn()) :?>
			<?php if($quantitaBirra == 0): ?>
				<strong>Non Disponibile</strong>
			<?php endif; ?>
			<?php if($quantitaBirra > 0): ?>
				<form onsubmit="return false;">
					<label for="quantity" class="hidden">Quantità</label>
					<input name="quantity" id="quantity" type="number" min="0" max="<?php echo $quantitaBirra;?>" value="<?php echo $defaultQuantity;?>"></input>
					<?php if(isUserLoggedIn()) : ?>
						<input type="submit" name="add" value="Aggiungi al carrello"/>
					<?php endif; ?>
					<?php if(!isUserLoggedIn()) : ?>
						<input type="submit" name="toLogin" value="Aggiungi al carrello"/>
					<?php endif; ?>			
				</form>
				<div class = "errors"><ul></ul></div>
			<?php endif; ?>
		<?php endif; ?>
	</section>
</section>
