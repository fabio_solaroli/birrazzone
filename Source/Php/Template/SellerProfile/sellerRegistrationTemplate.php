<h1>Registrazione birrificio</h1>
<section>
	<form onsubmit="return false;">
		<ul>
			<li class="label"><label for="ragioneSociale">Ragione sociale:</label></li>
			<li class="input"><input type="text" id="ragioneSociale" name="ragioneSociale" required/></li>
			
			<li class="label"><label for="email">Email:</label></li>
			<li class="input"><input type="email" id="email" name="email" required/></li>
			
			<li class="label"><label for="IVA">Partita IVA:</label></li>
			<li class="input"><input type="text" id="IVA" name="IVA" required/> </li>
	
			<li class="submit"><input type="submit" value="Invia la richiesta"/></li>
		</ul>
	</form>
	
	<div class="errors"><ul></ul></div>
</section>